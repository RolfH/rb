<?php

include_once './config.php';
// um nicht immer den code zu aendern um die mobile seite auf dem desktop auf zu rufen
$debugMobile = filter_input(INPUT_GET, 'dbm', FILTER_SANITIZE_STRING) != '';

// Beispiel für anoymous function in php
// bindet automatisch alle Klassen ein, die benötigt werden und im Ordner class vorhanden sind
spl_autoload_register(function ($class) {
    include './class/' . $class . '.php';
});
// gibt an was gefragt wird
$anfrage = isset($_POST['anfrage']) ? $_POST['anfrage'] : '';
$rezeptId = isset($_POST['id']) ? $_POST['id'] : ''; // Abfrage aus ausgabe.php

// wird noch entfernt
$rezepte = Rezept::alleRezepteMitAllenZutaten();

//$anfrage = isMobile()?

switch ($anfrage) {
    case 'gibEinRezept':
        $alles = Rezept::gibRezeptMitZutatenZurId($rezeptId);
        $view = 'jsonRezept';

        break;
    case '':

    default:
        $rezepte = Rezept::alleRezepteMitAllenZutaten();
        $anzahlRezepte = count($rezepte);
        $letzteRezeptId = $anzahlRezepte;
        $r = Rezept::gibLetztesRezeptMitZutaten();
        $view = 'ausgabe';
        break;
}


if (MobileDetect::isMobile()||$debugMobile) {
    include_once './view/mobile/Rezeptrechner.php';
} else {
    // hier kann die !mobile view bestimmt werden
    //$view = 'ausgabe1';
    // echo $_SERVER['HTTP_USER_AGENT']; DEBUG
    // view einbinden
    include 'view/' . $view . '.php';
}
?>
