<?php

Class MysqlHelper {
    /*
     * mysql_fetch_object funktioniert nicht immer mit Klassennamen,
     * jedoch zuverlässig mit stdClass, welchem die benötigten Methoden fehlen
     * 
     * castStdClassObjToClassObj überführt ein Objekt der stdClass
     * in ein Objekt der Klasse $classname, falls diese Klasse existiert
     * dieses Objekt wird zurueckgegeben
     */
    public static function castStdClassObjToClassObj(stdClass $std, $classname) {
        
        // neues Objekt der gewünschten Klasse erstellen
        $obj = new $classname;
        // um die Namen der Attribute zu ermitteln, die Reflektionsklasse aufrufen
        $reflect = new ReflectionClass($obj);
        
        // über alle Attribute der Zielklasse iterieren
        foreach ($reflect->getDefaultProperties() as $property => $value) {
            // setter für Zielklasse erzeugen
            $setter = "set". ucfirst($property);
            // falls es dieses Attribut in dem Objekt der stdClass gibt
            if (isset($std->{$property})){
                // dem Objekt in der Zielklasse den Wert des stdClass-Objekts
                //  zuweisen
                $obj->{$setter}($std->{$property});
            }
        }
        return $obj;
    }

}
