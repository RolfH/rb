<?php

class Rezept {
    /*
     * string, auf deutsch
     */

    public $name;
    /*
     * int, Anzahl der Personen, für die das Grundrezept geschrieben worden
     */
    public $anzahl;
    /*
     * Objekte der Klasse Zutat
     */
    public $zutaten = array();
    /*
     * PK aus db
     */
    public $id;

//
    function __construct($name = NULL, $anzahl = NULL, $id = NULL) {
        $this->name = $name;
        $this->anzahl = $anzahl;
        if ($id !== NULL) {
            $this->id = $id;
        }
    }

    public function addZutat(Zutat $z) {
        array_push($this->zutaten, $z);
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getAnzahl() {
        return $this->anzahl;
    }

    public function getZutaten() {
        return $this->zutaten;
    }

    public function removeZutaten() {
        $this->zutaten = array();
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setAnzahl($anzahl) {
        $this->anzahl = $anzahl;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public static function getAllohneZutaten($restriction = NULL) {
// Rückgabewert
        $rezepte = array();

        DbConnect::connect();
        $sql = "SELECT name, anzahl, id "
                . "FROM rezept "
                . "WHERE 1 ";
        if ($restriction !== NULL) {
            $sql .= $restriction;
        }

        $result = mysql_query($sql);

        $rezepte = array();

        while ($row = mysql_fetch_object($result)) {
            $rezept = MysqlHelper::castStdClassObjToClassObj($row, 'Rezept');
            array_push($rezepte, $rezept);
        }
        return $rezepte;
    }

// Uebergibt alle Rezepte mit jeweils allen Zutaten
    public static function alleRezepteMitAllenZutaten() {
        $rezepte = Rezept::getAllohneZutaten();
        mysql_query('SET CHARACTER SET utf8');
        for ($i = 0; $i < count($rezepte); $i++) {
            $z = new Zutat();
            $z->loadZutaten($rezepte[$i]);
        }
        return $rezepte;
    }

//public function gibLetztesRezeptMitZutaten() {
//$rezepte = $this->getAllohneZutaten( " id = (SELECT MAX(id) FROM rezepte) ");
//}

    /*
     * Rezept gab es noch gar nicht
     */
    public function save() {
// faslls nicht vor haden, dann ist es neues rezept
        if (!isset($this->id)) {

// Rezeptname muss zumindest existieren
// Rest kann später hinzugefügt werden
            if (isset($this->name)) {
                DbConnect::connect();
// falls anzahl nicht gesetzt, wird anzahl auf 0 gesetzt
                if (!isset($this->anzahl)) {
                    $this->anzahl = 0;
                }
                $sql = "INSERT INTO rezept ( name, anzahl) "
                        . "VALUES ('$this->name', $this->anzahl)";
                $result = mysql_query($sql);
                if ($result) {
                    $this->id = mysql_insert_id();
                }
// zum speichern brauche ich zumindest rezept_id

                foreach ($this->zutaten as $zutat) {
                    $zutat->save($this);
                }
            }
        } else {
// Rezeptname muss zumindest existieren
            if (isset($this->name)) {
// rezept-tabelle ändern
                DbConnect::connect();

                $sql = "UPDATE rezept "
                        . "SET name = '{$this->getName()}', "
                        . "anzahl = {$this->getAnzahl()} "
                        . "WHERE id={$this->getId()}";
                $success = mysql_query($sql);
                // Zutaten updaten
                // einfachster Weg: alle bisherigen Zutaten löschen
                Zutat::delete($this);
                // alle Zutaten des Rezepts neu speichern
                foreach ($this->zutaten as $zutat) {
                    $zutat->save($this);
                }
            }
        }
        Version::setVersion();
    }

    /*
     * Loeschen vom rezept
     */

    public function delete($id) {
        DbConnect::connect();
        $sql = "DELETE FROM rezept2zutat "
                . "WHERE rezept_id = " . $id;
        $success = mysql_query($sql);
        if ($success) {
            $sql = "DELETE FROM rezept "
                    . "WHERE id = " . $id;
            $success2 = mysql_query($sql);
        }
        return $success2;
        ;
    }

// Uebergibt ein bestimmtes Rezept per id
    public static function gibRezeptMitZutatenZurId($id) {
        $restriction = " AND id = $id";
        $rezepte = Rezept::getAllohneZutaten($restriction);
        $re = $rezepte[0];
        $z = new Zutat();
        $z->loadZutaten($re);

        return $re;
    }
    
    public static function gibLetztesRezeptMitZutaten(){
        $rezept = self::getAllohneZutaten(" AND id = ( "
                . "SELECT max(id) FROM rezept) ");
        Zutat::loadZutaten($rezept[0]);
        return $rezept[0];
    }
}
