<?php

class Zutat {
    /*
     * int, PK aus db
     */

    private $id;
    /*
     * float, gehoert zu einheit
     */
    public $menge;
    /*
     * string, Lebensmitteleinheit, wird in separater Tabelle gespeichert
     */
    public $einheit;
    /*
     * string auf deutsch
     */
    private $name;

    function __construct($menge = NULL, $einheit = NULL, $name = NULL, $id = NULL) {

        $this->menge = $menge;
        $this->einheit = $einheit;
        $this->name = $name;
        if ($id !== NULL) {
            $this->id = $id;
        }
        
    }

    public function getId() {
        return $this->id;
    }

    public function getMenge() {
        return $this->menge;
    }

    public function getEinheit() {
        return $this->einheit;
    }

    public function getName() {
        return $this->name;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setMenge($menge) {
        $this->menge = $menge;
    }

    public function setEinheit($einheit) {
        $this->einheit = $einheit;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public static function loadZutaten(Rezept $r) {
        // falls zuvor das Zutaten-Array nicht leer sein sollte
        $r->removeZutaten();
        DbConnect::connect();
        $sql = "SELECT z.id, z.name, r2z.menge, e.name einheit "
                . "FROM rezept r, zutat z, einheit e, rezept2zutat r2z "
                . "WHERE r2z.zutat_id = z.id "
                . "AND r2z.rezept_id=r.id "
                . "AND r2z.rezept_id=r.id "
                . "AND z.einheit_id=e.id "
                . "AND r.id = {$r->getId()}";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_object($result)) {
            $zutat = MysqlHelper::castStdClassObjToClassObj($row, 'Zutat');
            $r->addZutat($zutat);
        }
    }

    /*
     * speichert alle Zutaten zu gegebenem Rezept
     */

    public function save(rezept $r) {

        DbConnect::connect();
        // Zuordnungstabelle rezept2zutat soll gefuellt werden
        // bennoetige einheit_id
        $sql = "SELECT id "
                . "FROM  einheit "
                . "WHERE name = '{$this->getEinheit()}' ";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_assoc($result)) {
            $einheit_id = $row['id'];
        }
        // benoetige id des Zutatnamens
        $sql = "SELECT id "
                . "FROM zutat "
                . "WHERE name='{$this->getName()}'";
        $result = mysql_query($sql);
        if (mysql_num_rows($result) == 0) {
            // neuer Zutatname muss erstellt werden
            $sql = "INSERT INTO zutat (name, einheit_id) "
                    . "VALUES('{$this->getName()}', $einheit_id)";
            $result = mysql_query($sql);
            if ($result) {
                $zutat_id = mysql_insert_id();
            }
        } else {
            $result = mysql_query($sql);
            while ($row = mysql_fetch_assoc($result)) {
                $zutat_id = $row['id'];
            }
        }

        // fuellen von rezept2zutat
        $sql = "INSERT INTO rezept2zutat(menge, rezept_id, zutat_id) "
                . "VALUES({$this->getMenge()}, {$r->getId()}, $zutat_id) ";
        $success = mysql_query($sql);
        if ($success) {
            $this->setId(mysql_insert_id());
        }
    }

    public static function delete(Rezept $r) {
        DbConnect::connect();
        // nur Daten in Zuordnungstabelle löschen
        $sql = "DELETE FROM rezept2zutat "
                . "WHERE rezept_id = {$r->getId()}";
        $success = mysql_query($sql);
        return $success;
    }

}

//$sql = "SELECT z.id, r2z.einheit, e.name, r2z.menge "
//                . "FROM rezept r, zutat z, einheit e, rezept2zutat r2z "
//                . "WHERE r2z.zutat_id = z.id "
//                . "AND r2z.rezept_id=r.id "
//                . "AND r2z.rezept_id=r.id "
//                . "AND z.einheit_id=e.id "
//                . "AND r.id = {$r->getId()}"; echo $sql;
