<?php

class MobileDetect {

    public static function isMobile() {
        $client = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_STRING);

        if ($client === null) {
            $client = '';
        }

        $mobiles = ['iphone', 'ipad', 'blackberry', 'playbook', 'kindle', 'silk', 'android', 'windows phone', 'webos'];
        $result = $client === '' ? TRUE : FALSE; // wenn der $client leer ist wird eine cache Anfrage gestellt

        for ($i = 0; $i < count($mobiles); $i++) {
            $pattern = "/" . $mobiles[$i] . "/i";
            if (preg_match($pattern, $client)) {
                $result = TRUE;
            }
        }

        return $result;
    }

}
