<?php

class RezeptPublic {
    public $id;
    public $name;
    public $anzahl;
    public $zutaten = array();
    
    public function __construct($id,$name,$anzahl,$zutaten) {
        $this->id = $id;
        $this->name = $name;
        $this->anzahl = $anzahl;
        for ($i=0;$i<count($zutaten);$i++) {
            $this->zutatEinfuegen($zutaten[$i]);
        }
    }
    
    public function zutatEinfuegen($zutat) {
        array_push($this->zutaten, new ZutatPublic($zutat->getId(),$zutat->getName(),$zutat->getEinheit(),$zutat->getMenge()));
    }
    
    public static function PublicRezepte($rezeptArray) {
        $res = array();
        for ($i=0;$i<count($rezeptArray);$i++) {
            $id = $rezeptArray[$i]->getId();
            $name = $rezeptArray[$i]->getName();
            $anzahl = $rezeptArray[$i]->getAnzahl();
            $zutaten = $rezeptArray[$i]->getZutaten();
            array_push($res, new RezeptPublic($id,$name,$anzahl,$zutaten));
        }
        return $res;
    }
}

class ZutatPublic {
    public $id;
    public $name;
    public $einheit;
    public $menge;
    
    public function __construct($id, $name, $einheit, $menge) {
        $this->id = $id;
        $this->name = $name;
        $this->einheit = $einheit;
        $this->menge = $menge;
    }
}