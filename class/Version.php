<?php
class Version {

    public static function setVersion() {
        DbConnect::connect();
        $sql = "INSERT INTO version (zeitstempel) 
            VALUES(now())";
        $success = mysql_query($sql);
        if ($success) {
            // appcache wird aktualisiert
            Version::writeVersionIntoAppManifest();
        }
        return $success;
    }
    /*
     * gibt letzten Zeitstempel im Mysql-Format zurueck,
     * der durch Änderung in anderen Tabellen dieser db erzeugt wurde 
     */
    public static function getVersion(){
        DbConnect::connect();
        $sql = "SELECT MAX(zeitstempel) stempel "
                . "FROM version ";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            return $row['stempel'];
        }
    }
    
    public static function writeVersionIntoAppManifest() {
        $file_name = './mobileCache/manifest.appcache';
        $version = Version::getVersion();
        if(file_exists($file_name)) {
            $handle = fopen($file_name, "a");
            $lines = file($file_name);
            $lines[1] = "# " . $version . "\n";
            ftruncate($handle, 0);
            for ($i=0;$i<count($lines);$i++) {
                fwrite($handle, $lines[$i]);
            }
            fclose($handle);
        } else {
            echo 'Fehler: manifest.appcache nicht gefunden';
        }
    }

}
