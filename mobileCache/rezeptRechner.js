var rezept = '';
var view = 'auswahl';
var loaded = false;
var appCache = window.applicationCache;
var version = '1.0';
if ((typeof rezepte[0]) === "object") {
    console.log('keine neuen Rezepte');
    rezept = rezepte[0];
} else {
    var tmp = [];
    for (var i=1;i<rezepte.length;i++) {
        tmp.push(rezepte[i]);
    }
    version = rezepte[0];
    rezepte = tmp;
}

$(document).ready(function() {
    window.addEventListener('load', function(e) {

        window.applicationCache.addEventListener('updateready', function(e) {
            if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
                // Browser downloaded a new app cache.
                // Swap it in and reload the page to get the new hotness.
                window.applicationCache.swapCache();
                /*if (confirm('A new version of this site is available. Load it?')) {
                    window.location.reload();
                }*/
                if (view === 'auswahl') {
                    window.location.reload();
                } 
            } else {
                // Manifest didn't changed. Nothing new to server.
            }
        }, false);

    }, false);
    
    $('#version').text("Rezeptrechner, version: " + version);

    switch (view) {
        case 'auswahl':
            if (rezepte.length > 0) {
                for (var i = 0; i < rezepte.length; i++) {
                    $('#rezeptListe').append('<li><a href="#Mengenberechnung" onclick="setRezept(this)">' + rezepte[i].name + '</a></li>');
                }
                $('#rezeptListe').listview('refresh');
            } else {
                $('#error').val('Fehler: keine Rezepte im Cache');
            }
            break;
        case 'rechnen':
            // catch list error
            break;
    }
});

function getRezeptZahl() {
    console.log(rezepte.length);
}

function setRezept(elem) {
    for (var i = 0; i < rezepte.length; i++) {
        // da Rezeptnamen "unique" sind kann danach unterschieden werden
        if (rezepte[i].name == elem.innerHTML) {
            rezept = rezepte[i];
        }
    }
    $('#rezeptNameOben').text(rezept.name);
    $('#rezeptNameUnten').text(rezept.name);
    $('#anzahl').val(rezept.anzahl);
    fillTable();
    view = 'rechnen';
}

function fillTable() {
    var parent = $('#table');
    var zutaten = rezept.zutaten;
    if (!loaded) {
        loaded = true;
        for (var i = 0; i < zutaten.length; i++) {
            $(parent).append('<tr style="border-bottom: 2px solid black;"><td>' + zutaten[i].menge + '</td><td>' + zutaten[i].einheit + '</td><td>' + zutaten[i].name + '</td></tr>');
        }
    }
    try {
        $('table').table('refresh');
    } catch (e) {
        
    }
}

function emptyTable() {
    $('#table').empty();
    view = 'auswahl';
    loaded = false;
    window.location.hash = '#Rezeptauswahl';
}

function rechnen() {
    if ($('#anzahl').val() > 0) {
        var factor = $('#anzahl').val() / rezept.anzahl;
        var zutaten = rezept.zutaten;
        for (var i = 0; i < zutaten.length; i++) {
            $('#table tr td:first-child').each(function(i) {
                $(this).first().text(Math.round(zutaten[i].menge * factor * 10000) / 10000);
            });
        }
    } else {
        $('#anzahl').val(rezpet.anzahl);
    }
    $('table').table('refresh');
}

$(window).on("navigate", function(event, data) {
    // leeren der Tabelle wenn der Benutzer den Zurückknopf drückt
    var direction = data.state.direction;
    if (direction == 'back') {
        emptyTable();
    }
});

function onUpdateReady() {
    appCache.swapCache();
}

appCache.addEventListener('updateready', onUpdateReady);
if (window.applicationCache.status === window.applicationCache.UPDATEREADY) {
    onUpdateReady();
}