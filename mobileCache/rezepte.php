<?php 

include_once '../config.php';

spl_autoload_register(function ($class) {
    include '../class/' . $class . '.php';
});

$rezepte = Rezept::alleRezepteMitAllenZutaten();
$publicRezepte = RezeptPublic::PublicRezepte($rezepte);

echo 'var rezepte =' . json_encode(versionAnhaengen($publicRezepte));

function versionAnhaengen($rezepte) {
    // version der Rezepte
    $version = Version::getVersion();
    $tmp = [$version];
    for ($i=0;$i<count($rezepte);$i++){
        array_push($tmp, $rezepte[$i]);
    }
    return $tmp;
}

