<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Rezept - Verwaltung</title>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script type="text/css">
        #nav {
            color:"red";
        }
        </script>
        <script>
        $(document).ready(function ()
        {
            $('#ausWahl').change(function ()
            {
                $('#ausWahl option:selected').each(function ()
                {
                   var anfrage = 'gibEinRezept';
                   var rezeptName = ($(this).text()); 
                   var rezeptId = ($(this).attr('id'));
                   $("#Rname").val(rezeptName);
                   $.post('index.php',
                   {
                       anfrage:anfrage,
                       id:rezeptId  
                   },
                   function (data,status)
                   {
                       var result = $.parseJSON(data);
                       $("#anzahlPerson").val(result[0].anzahl);
                       $("#test").empty();
                       for (var i = 0 ; i < result[0].zutaten.length; i++)
                       {
                           $("#test").append('<tr><td><input type="text" name="" value="'+result[0].zutaten[i].menge+'"/></td><td><input type="text" name="" value="'+result[0].zutaten[i].einheit+'"/></td><td><input type="text" name="" value="'+result[0].zutaten[i].name+'"/></td></tr>');
                       }
                   });
                });
            });
        });
    </script>
    </head>
    <body>
        <nav>

        </nav>
        <div id="content">
            <div id="header"> Rezeptausgabe </div>  
            <form action="index.php"> 
            <div id="right">
                <table>
                <input id="Rname" type="text" name="name" value="<?= $r->getName() ?>"/>
                    <br/>
                    <input id="anzahlPerson" type="text" name="anzahl" value="<?= $r->getAnzahl() ?>"/>
                    <input type="button" name="ändern" />
                    <br/>
                    <tr>
                        <th>Menge</th>
                        <th>Einheit</th>
                        <th>Zut</th>
                    </tr>
                    <tbody id="test">
                        <?php for ($i = 0; $i < count($r->getZutaten()); $i++) {  ?>  
                         
                       
                        <tr><td><input type="text" name="" value="<?= $r->getZutaten()[$i]->getEinheit() ?>"/></td>
                            <td><input type="text" name="" value="<?= $r->getZutaten()[$i]->getMenge() ?>"/></td>
                            <td><input type="text" name="" value="<?= $r->getZutaten()[$i]->getName() ?>"/></td></tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <div id="left">
                <select id="ausWahl" name="rezepte" size="<?= $anzahlRezepte; ?>">
                <?php for($i = 0; $i < $anzahlRezepte; $i++){
                    echo "<option id='" . $rezepte[$i]->getId() . "'>". $rezepte[$i]->getName() ."</option>";
                }
               ?>
            </select>
            </div>
          </form>  
        </div>
        
    </body>
</html>
