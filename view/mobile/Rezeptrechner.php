<!DOCTYPE html>
<html manifest="/../../rb/mobileCache/manifest.appcache">
    <head>
        <!-- Sollte das Projekt auf einem richtigem Server online gestellt werden muss ' . "/rb/"' entfernt werden -->
        <base href="<?= "http://" . $_SERVER['HTTP_HOST'] . "/rb/" ?>" target="_blank" />
        <meta name="viewport" content="minimum-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no" />
        <meta charset="utf-8" />
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.css">
        <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.js"></script>
        <script src="mobileCache/rezepte.php" type="text/javascript"></script>
        <script src="mobileCache/rezeptRechner.js" type="text/javascript"></script>
        <style>
            table {
                display: table;
            }
        </style>
        <title>Rezeptrechner</title>
    </head>
    <body>

        <div data-role="page" id="Rezeptauswahl">
            <div data-role="header">
                <h2>Rezeptauswahl</h2>
            </div>
            <div data-role="main" class="ui-content">
                
                <ul data-role="listview" data-autodividers="true" data-inset="true" id="rezeptListe">
                </ul>
                
            </div>
            <div data-role="footer" data-position="fixed">
                <h1 id="version"></h1>
            </div>
        </div> 

        <div data-role="page" id="Mengenberechnung" data-title="Rezeptrechner">
            <div data-role="header">
                <h1 id="rezeptNameOben"></h1>
            </div>

            <div data-role="main" class="ui-content">
                <div class="ui-grid-b">
                    <div style="text-align: right; padding-right: 10px;" class="ui-block-a personen">für</div>
                    <div class="ui-block-b personen"><input type="number" id="anzahl" value="" /></div>
                    <div style="text-align: left; padding-left: 10px;" class="ui-block-c personen"><button onclick="rechnen()">Personen berechnen</button></div>
                </div>
            </div>
            <div class="table-container">
                <table data-role="table" class="ui-table-reflow tableView" style="border-collapse: collapse;" data-mode="reflow">
                    <thead>

                        <tr>
                            <th>Menge</th>
                            <th>Einheit</th>
                            <th>Zutat</th>
                        </tr>

                    </thead>
                    <tbody id="table">

                    </tbody>
                </table>
            </div>
            <div class="ui-grid-b">
                <div class="ui-block-a tableHead"><button onclick="emptyTable()">Zurück</button></div>
                <div class="ui-block-b tableHead"></div>
                <div class="ui-block-c tableHead"></div>
            </div>
            <div data-role="footer" data-position="fixed">
                <h1 id="rezeptNameUnten"></h1>
            </div>
        </div>
    </body>
</html>
