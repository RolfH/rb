<?php
spl_autoload_register(function ($class) {
    include '../class/' . $class . '.php';
});

$allesNew = RezeptPublic::PublicRezepte([$alles]);
$data = json_encode($allesNew);
echo $data;

$view = 'ausgabe';
