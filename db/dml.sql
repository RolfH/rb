INSERT INTO einheit (id, name) VALUES
(6, 'Bund'),
(5, 'Dose'),
(4, 'g'),
(1, 'kg'),
(3, 'l'),
(2, 'Stück');

INSERT INTO rezept (id, name, anzahl) VALUES
(1, 'Gulasch', 4),
(2, 'Spaghetti Bolognese', 8),
(3, 'Pellkartoffeln mit Quark', 2);

INSERT INTO zutat (id, einheit_id, name) VALUES
(1, 1, 'Fleischwürfel'),
(2, 2, 'Metzgerzwiebel'),
(3, 3, 'Öl'),
(4, 4, 'Mehl'),
(5, 5, 'Tomaten'),
(6, 4, 'Paprikapulver'),
(7, 4, 'Pfeffer, schwarz'),
(8, 4, 'Salz'),
(9, 1, 'Hackfleisch'),
(10, 6, 'Petersilie, schwarz'),
(11, 4, 'Parmesan'),
(12, 4, 'Spaghetti'),
(13, 4, 'Tomatenmark'),
(14, 4, 'Kartoffeln'),
(15, 4, 'Quark');

INSERT INTO rezept2zutat (id, rezept_id, zutat_id, menge) VALUES
(1, 1, 1, 1),
(2, 1, 2, 4),
(3, 1, 3, 0.2),
(4, 1, 4, 100),
(5, 1, 5, 1),
(6, 1, 6, 30),
(7, 1, 7, 5),
(8, 1, 8, 20),
(9, 2, 9, 1),
(10, 2, 8, 60),
(11, 2, 3, 0.5),
(12, 2, 2, 10),
(13, 2, 5, 1),
(14, 2, 10, 2),
(15, 2, 11, 400),
(16, 2, 12, 1000),
(17, 2, 13, 40),
(18, 3, 14, 500),
(19, 3, 8, 100),
(20, 3, 15, 250),
(21, 3, 10, 0.3);

INSERT INTO version (id, zeitstempel) VALUES
(1, '2014-09-03 14:02:30');