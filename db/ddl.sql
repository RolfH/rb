

-- Host: localhost
-- Erstellungszeit: 21. Aug 2014 um 14:17
-- Server Version: 5.6.12
-- PHP-Version: 5.5.3

--
-- Datenbank: `rb`
--
DROP DATABASE IF EXISTS rb;
CREATE DATABASE IF NOT EXISTS rb DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE rb;

-- --------------------------------------------------------

--
-- Tabellenstrukturen
--

CREATE TABLE IF NOT EXISTS einheit (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(25) COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY name (name)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS rezept (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(45) COLLATE utf8_general_ci NOT NULL,
  anzahl int(11) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY name (name)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS rezept2zutat (
  id int(11) NOT NULL AUTO_INCREMENT,
  rezept_id int(11) NOT NULL,
  zutat_id int(11) NOT NULL,
  menge float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY rezept_id (rezept_id),
  KEY zutat_id (zutat_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS zutat (
  id int(11) NOT NULL AUTO_INCREMENT,
  einheit_id int(25) NOT NULL,
  name varchar(45) COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (id),
  KEY einheit_id (einheit_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1 ;





CREATE TABLE IF NOT EXISTS version (
  id int(11) NOT NULL AUTO_INCREMENT,
  zeitstempel datetime NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;






-- CONSTRAINTS

ALTER TABLE rezept2zutat
  ADD CONSTRAINT rezept2zutat_ibfk_2 FOREIGN KEY (zutat_id) REFERENCES zutat (id),
  ADD CONSTRAINT rezept2zutat_ibfk_1 FOREIGN KEY (rezept_id) REFERENCES rezept (id);

ALTER TABLE zutat
  ADD CONSTRAINT zutat_ibfk_1 FOREIGN KEY (einheit_id) REFERENCES einheit (id);